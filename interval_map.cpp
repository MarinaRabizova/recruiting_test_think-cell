#include <map>
template<typename K, typename V>
class interval_map {
	friend void IntervalMapTest();

    // TODO m_valBegin and m_map MUST BE HERE
public:
    // Temp fix. Just for debug
    V m_valBegin;
    std::map<K,V> m_map;

	// constructor associates whole range of K with val
	interval_map(V const& val)
	: m_valBegin(val)
	{}

	// Assign value val to interval [keyBegin, keyEnd).
	// Overwrite previous values in this interval.
	// Conforming to the C++ Standard Library conventions, the interval
	// includes keyBegin, but excludes keyEnd.
	// If !( keyBegin < keyEnd ), this designates an empty interval,
	// and assign must do nothing.
	void assign( K const& keyBegin, K const& keyEnd, V const& val ) {
// INSERT YOUR SOLUTION HERE
        printf("######### Assign started! #########\n");
        printf("Vars:\nkeyBegin=%d;\nkeyEnd=%d,\nval=%c\n", keyBegin, keyEnd, val);

        if (!(keyBegin < keyEnd)) {
            printf("Incorrect value!!!\n");
            return;
        }

        for (int i = keyBegin; i < keyEnd; i++) {
            if (m_map.insert(std::pair<K, V>(i, val)).second == false) {
                m_map.erase(i);
                m_map.insert(std::pair<K, V>(i, val));
            }
        }
        m_map.insert(std::pair<K, V>(keyEnd, m_valBegin));

        printf("######### Assign ended! #########\n");
	}

	// look-up of the value associated with key
	V const& operator[]( K const& key ) const {
		auto it=m_map.upper_bound(key);
		if(it==m_map.begin()) {
			return m_valBegin;
		} else {
			return (--it)->second;
		}
	}

};

// Many solutions we receive are incorrect. Consider using a randomized test
// to discover the cases that your implementation does not handle correctly.
// We recommend to implement a test function that tests the functionality of
// the interval_map, for example using a map of int intervals to char.